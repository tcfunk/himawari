-- Make enemies or whatever patrol back and forth
-- 


local PatrollingSystem = Tiny.processingSystem()
PatrollingSystem.filter = Tiny.requireAll('patrols')

function PatrollingSystem:process(e, dt)
	local percent = (math.sin(love.timer.getTime()) / 2) + 0.5

	e.pos.x = lume.smooth(e.a.x, e.b.x, percent) 
	e.pos.y = lume.smooth(e.a.y, e.b.y, percent) 
end


return PatrollingSystem
