-- Draw circles for temp art!
-- 


local CircleSystem = Tiny.processingSystem()
CircleSystem.filter = Tiny.requireAll('radius', 'color')

function CircleSystem:process(e, dt)
	love.graphics.setColor(e.color)
	love.graphics.circle('fill', love.window.toPixels(e.pos.x), love.window.toPixels(e.pos.y), love.window.toPixels(e.radius))
	love.graphics.setColor(255, 255, 255)
end


return CircleSystem

