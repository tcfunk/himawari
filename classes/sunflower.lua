Sunflower = Class {
    init = function(self)
			local w, h, _ = love.window.getMode()
			self.pos = Vec(w/2, h - self.radius*2)
    end,

		radius = 50,
		color = {200, 200, 50},
		speed = 100,

		touch = {},
}

function Sunflower:update(dt)

	local movement = Vec(0, 0)

	-- Try keyboard controls
	if love.keyboard.isDown('left') or love.keyboard.isDown('a') then
		movement.x = -1
	elseif love.keyboard.isDown('right') or love.keyboard.isDown('d') then
		movement.x = 1
	end
	if love.keyboard.isDown('up') or love.keyboard.isDown('w') then
		movement.y = -1
	elseif love.keyboard.isDown('down') or love.keyboard.isDown('s') then
		movement.y = 1
	end

	-- Try touch/mouse-based controls
	if movement.x == 0 and movement.y == 0 then
		if love.mouse.isDown(1) then
			movement.x = love.mouse.getX() - love.window.toPixels(self.pos.x)
			movement.y = love.mouse.getY() - love.window.toPixels(self.pos.y)
		else
			local touches = love.touch.getTouches()
			if #touches ~= 0 then
				local x, y = love.touch.getPosition(touches[1])
				movement.x = x - love.window.toPixels(self.pos.x)
				movement.y = y - love.window.toPixels(self.pos.y)
			end
		end
	end

	-- Move player according to input, frame time, and splayer speed
	self.pos = self.pos + movement:normalized() * self.speed * dt
end

function Sunflower:mousemoved(x, y, dx, dy, istouch)
end

function Sunflower:mousepressed(x, y, button, istouch)
end

function Sunflower:mousereleased(x, y, button, istouch)
end

