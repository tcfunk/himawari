function love.conf(t)
	local ratio = 9/16;
  t.window.width = 400
  --t.window.height = 910
	t.window.height = 400/ratio;
  t.window.resizable = false
  t.window.display = 1
	t.window.highdpi = true

  t.gammacorrect = true
end
