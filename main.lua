
function love.load()

  -- Load global modules
  Tiny = require 'lib.tiny'
  Camera = require 'lib.camera'
  GS = require 'lib.gamestate'
  Class = require 'lib.class'
  Vec = require 'lib.vector'
  Vivid = require 'lib.vivid'
  Signal = require 'lib.signal'
  lume = require 'lib.lume'

  -- Initialize asset folder
  assets = require('lib.cargo').init('assets')

  -- Load up title screen
  GS.registerEvents()
	GS.registerEvents { 'touchpressed', 'touchreleased', 'touchmoved' }
  GS.switch(require('scenes.title'))
end

function love.draw()
end

function love.update(dt)
end

function love.keypressed(k, s, r)

  -- Mostly for testing purposes...should probably remove
  -- this shortcut after shipping
  if s == 'escape' then
    love.event.quit()
  end

end

function love.touchpressed(id, x, y, dx, dy, pressure)
	print('touchpressed')
	GS.touchpressed(id, x, y, dx, dy, pressure)
end
