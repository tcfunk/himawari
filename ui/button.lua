--------------------------------------------------------------------------------
-- Button class
--
-- @TODO:
-- |- Track state
-- |  |- Draw differently depending on button state
-- |
-- |- Handle line-wrapping when drawing text
--
--
-- @param: pos - expects a table {x=, y=}
-- @param: width - int
-- @param: height - int
-- @param: text - expects a love.Text object
-- @param: img - expects a love.Image object
--------------------------------------------------------------------------------

Button = Class {
  init = function(self, pos, width, height, text, img)
    self.w = width
    self.h = height
    self:setPosition(pos)

    -- These can be changed with setBackgroundColor and setColor
    self.backgroundColor = {0, 0, 0}
    self:setColor({255, 255, 255})

    -- Create text object if provided
    self.text = text

    -- "state machine" aka strings
    self.state = 'default'

    -- Create image object if provided
    if img ~= nil then
      self.img = love.graphics.newImage(img)
    end

  end
}

function Button:draw()

  -- Draw button
  love.graphics.setColor(self:getBackgroundColor())
  local x, y = self.posOffset:unpack()
  love.graphics.rectangle('fill', x, y, self.w, self.h, 5, 5)
  love.graphics.setColor({0, 0, 0})
  love.graphics.rectangle('line', x, y, self.w, self.h, 5, 5)

  -- Draw button text
  if self.text ~= nil then
    love.graphics.setColor(self.color)
    x, y = self.pos:unpack()
    local w, h = self.text:getDimensions()
    love.graphics.draw(self.text, x, y, 0, 1, 1, w/2, h/2)
  end

  -- Reset color
  love.graphics.setColor(255, 255, 255, 255)
end

function Button:isMouseOver(x, y)
  local isHovered = x > self.posOffset.x and
                    x < self.posOffset.x + self.w and
                    y > self.posOffset.y and
                    y < self.posOffset.y + self.h

  if isHovered then
    if self.state ~= 'pressed' then
      if self.state == 'hot' then
        self.state = 'pressed'
      else
        self.state = 'hovered'
      end
    end
  else
    if self.state == 'pressed' or self.state == 'hot' then
      self.state = 'hot'
    else
      self.state = 'default'
    end
  end

  return isHovered
end

function Button:hover()
end

function Button:isPressed()
  return self.state == 'pressed'
end

function Button:press()
  self.state = 'pressed'
end

-- Unpress the button, without firing the event
-- (used when the mouse is released but is no longer over the button)
function Button:depress()
  self.state = 'default'
end

function Button:release()
  if self.state == 'pressed' then
    self.action()
  end

  -- Mouse is probably over the button, still
  self.state = 'hovered'
end

function Button:setPosition(pos)
  self.pos = pos
  self.posOffset = pos - Vec(self.w/2, self.h/2)
end

function Button:setBackgroundColor(color)
  self.backgroundColor = color
  self.activeColor = {Vivid.darken(0.01, self.backgroundColor)}
  self.hoverColor = {Vivid.lighten(0.01, self.backgroundColor)}
end

function Button:getBackgroundColor()
  if self.state == 'hovered' then
    return self.hoverColor
  elseif self.state == 'pressed' then
    return self.activeColor
  else
    return self.backgroundColor
  end
end

function Button:setColor(color)
  self.color = color
end

function Button:setAction(fn)
  self.action = fn
end
