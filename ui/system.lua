local ui = {
  components = {}
}

function ui:register(component)
  table.insert(ui.components, component)
end

function ui:draw()
  for i, component in ipairs(ui.components) do
    component:draw()
  end
end

function ui:keypressed(k, s, r)
end

function ui:keyreleased(k, s, r)
end

function ui:mousemoved(x, y, dx, dy, istouch)
  for i, component in ipairs(ui.components) do
    if component:isMouseOver(x, y) then
      component:hover()
      return
    end
  end
end

function ui:mousepressed(x, y, button, istouch)
  for i, component in ipairs(ui.components) do
    if component:isMouseOver(x, y) then
      component:press()
      return
    end
  end
end

function ui:mousereleased(x, y, button, istouch)
  for i, component in ipairs(ui.components) do
    if component:isMouseOver(x, y) and component:isPressed() then
      component:release()
    else
      component:depress()
    end
  end
end

return ui
