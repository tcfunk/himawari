local scene = {
  name = 'Main Game',
}

require 'classes.crow'
require 'classes.sunflower'

function scene:init()
  self.ui = require 'ui.system'
	self.player = Sunflower()

	scene.world = Tiny.world(
		self.player,
		Crow(Vec(10, 10), Vec(200, 10)),
		Crow(Vec(200, 40), Vec(10, 40)),
		require 'systems.circle',
		require 'systems.patrolling'
	)
end

function scene:enter()
end

function scene:update(dt)
	self.player:update(dt)
end

function scene:draw()
	scene.world:update(love.timer.getDelta())
end

function scene:mousemoved(x, y, dx, dy, istouch)
  self.ui:mousemoved(x, y, dx, dy, istouch)
	self.player:mousemoved(x, y, dx, dy, istouch)
end

function scene:mousepressed(x, y, button, istouch)
  self.ui:mousepressed(x, y, button, istouch)
	self.player:mousepressed(x, y, button, istouch)
end

function scene:mousereleased(x, y, button, istouch)
  self.ui:mousereleased(x, y, button, istouch)
	self.player:mousereleased(x, y, button, istouch)
end

function scene:touchpressed(id, x, y, dx, dy, pressure)
	print('touchpressed')
end

function scene:touchreleased(id, x, y, dx, dy, pressure)
	print('touchreleased')
end

function scene:touchmoved(id, x, y, dx, dy, pressure)
	print('touchmoved')
end


return scene

