require 'ui.button'


local scene = {
  name = 'Title Screen'
}

function scene:init()

  local w, h = love.graphics.getDimensions()
  local text = love.graphics.newText(assets.fonts.kpb(love.window.toPixels(24)), 'PLAY!')
  local rw, rh = love.window.toPixels(350), love.window.toPixels(600)

  -- UI instance
  self.ui = require 'ui.system'

  -- Play! button
	local bw, bh = love.window.toPixels(200), love.window.toPixels(75)
  local startButton = Button(Vec(w/2, (h+rh)/2 - bh), bw, bh, text)
  startButton:setColor({255, 255, 255})
  startButton:setBackgroundColor({20, 122, 188})
  startButton:setAction(function()
      Signal.emit('play')
  end)
  self.ui:register(startButton)

  -- @TODO: instructions button

  self.himawari = love.graphics.newText(assets.fonts.kpb(love.window.toPixels(24)), "-\ns\nu\nn\nf\nl\no\nw\ne\nr\n-")

  -- Set background color for sky
  love.graphics.setBackgroundColor(131, 192, 240)
end

function scene:enter()
  Signal.register('play', function()
    GS.switch(require('scenes.play'))
  end)
end

function scene:draw()
  local w, h = love.graphics.getDimensions()

  -- menu background
  love.graphics.setColor(165, 234, 189)
  local rw, rh = love.window.toPixels(350), love.window.toPixels(600)
  local x, y = (w-rw)/2, (h-rh)/2
  love.graphics.rectangle('fill', x, y, rw, rh, 5, 5)
  love.graphics.setColor(0, 0, 0)
  love.graphics.rectangle('line', x, y, rw, rh, 5, 5)

  -- himawari (english)
  local tw, th = self.himawari:getDimensions()
  x, y = (w+rw)/2 - tw - 5, y
  love.graphics.draw(self.himawari, x, y)

  -- himawari (japanese)
	-- @TODO: get 2x res img for mobile
  local iw, ih = assets.img.himawari_h:getDimensions()
  x = x - iw
  love.graphics.draw(assets.img.himawari_h, x, y+10, 0, 1, 1, iw/2, 0)


  -- sunflower
	-- @TODO: get 2x res img for mobile
  love.graphics.setColor(255, 255, 255)
  local iw, ih = assets.img.sunflower:getDimensions()
  x, y = (w-rw)/2, (h-rh)/2
  love.graphics.draw(assets.img.sunflower, x, y, 0, 2, 2, iw/3, ih/3)

  -- draw UI
  self.ui:draw()
end

function scene:mousemoved(x, y, dx, dy, istouch)
  self.ui:mousemoved(x, y, dx, dy, istouch)
end

function scene:mousepressed(x, y, button, istouch)
  self.ui:mousepressed(x, y, button, istouch)
end

function scene:mousereleased(x, y, button, istouch)
  self.ui:mousereleased(x, y, button, istouch)
end

function scene:keypressed(k, s, r)
	if s == 'return' then
		GS.switch(require('scenes.play'))
	end
end


return scene
